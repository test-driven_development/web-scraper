
1. **Story** : As a user, I want to specify a URL, so that the scraper can target a specific web page.

* **Given** I have a valid URL
* **When** I input the URL into the web scraper
* **Then** the scraper should target the web page at that URL

1. **Story** : As a user, I want to extract specific data (like text, links, images) from the page, so that I can collect only the information I need.

* **Given** the scraper is targeting a specific web page
* **When** I specify the type of data to be extracted (e.g., text, links, images)
* **Then** the scraper should extract only that type of data from the page

1. **Story** : As a user, I want to save the extracted data in a structured format (like CSV, JSON), so that it's easy to use and analyze later.

* **Given** data has been extracted from a web page
* **When** I choose a format (CSV or JSON) for the data
* **Then** the scraper should save the data in that format
