import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class WebScraper:
    def __init__(self):
        self.url = None
        self.data = None

    def set_url(self, url):
        self.url = url

    def get_url(self):
        return self.url
    
    def get_data(self):
        return self.data

    def scrape_data(self, data_type):
        # Fetch the web page content
        response = requests.get(self.url)
        soup = BeautifulSoup(response.content, 'html.parser')

        if data_type == 'text':
            self.data = soup.get_text()
        elif data_type == 'links':
            self.data = [link.get('href') for link in soup.find_all('a')]
        elif data_type == 'images':
            self.data = [img.get('src') for img in soup.find_all('img')]

    def scrape_paginated_data(self, data_type, link_text):
        self.data = []
        current_url = self.url

        while current_url:
            response = requests.get(current_url)
            soup = BeautifulSoup(response.content, 'html.parser')

            if data_type == 'text':
                self.data.append(soup.get_text())

            # Find the next page link by link text
            next_page_link = soup.find('a', text=link_text)
            current_url = next_page_link.get('href') if next_page_link else None


    def scrape_javascript_rendered_data(self, data_type):
        # Set up Selenium with headless Chrome
        options = Options()
        options.headless = True
        driver = webdriver.Chrome(options=options)

        driver.get(self.url)
        # Wait or interact as needed to render the page fully
        # ...

        # Now use BeautifulSoup to parse the rendered HTML
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        if data_type == 'text':
            self.data = soup.get_text()

        driver.quit()