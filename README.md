# Web Scraper

## Getting started

Experience the cutting-edge capabilities of the GPT-powered [TDD &amp; BDD Assistant](https://chat.openai.com/g/g-tYHrfycNg-tdd-bdd-assistant) as we embark on a journey to [develop](https://shareg.pt/rNRnXfB) a sophisticated Web Scraper. This project not only showcases the innovative prowess of our application but also demonstrates the effective application of Test-Driven Development (TDD) and Behavior-Driven Development (BDD) methodologies. Dive into a real-world scenario where precision, efficiency, and user-centric design come to life through the power of TDD and BDD practices. Join us in redefining software development standards.

## Docker

`docker build -t registry.gitlab.com/test-driven_development/web-scraper .`

`docker run -it -v $(pwd):/container-workdir registry.gitlab.com/test-driven_development/web-scraper /bin/bash`

`docker push registry.gitlab.com/test-driven_development/web-scraper`
